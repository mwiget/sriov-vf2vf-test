#!/bin/bash
if [ $(id -u) -ne 0 ]; then 
  echo "please run as root" 
  exit 1
fi
mst start

for PF in enp1s0f0np0 enp1s0f1np1; do
  echo -n $PF ... 
  mlxconfig -d /dev/mst/mt4121_pciconf0 q|grep NUM_OF_VF
  mlxconfig -d /dev/mst/mt4121_pciconf0 -y set SRIOV_EN=1 NUM_OF_VFS=4
  echo -n $PF ... 
  cat /sys/class/net/enp1s0f0np0/device/sriov_totalvfs
done
