# sriov-vf2vf-test

Simple connectivity test between VFs on the same PF with SR-IOV on a Mellanox ConnectX-5 on Fedora 35.
The PF is connected via loopback cable to the second port of the same NIC and both ports must be up.
While one can simply connect the port to an Ethernet switch, the goal here is to proof VF to VF connectivity
within the NIC instead of having traffic switched back by a switch (though most won't switches don't send
traffic back out the same port a packet came in...). 

```
$ uname -a
Linux rog1 5.14.18-300.fc35.x86_64 #1 SMP Fri Nov 12 16:43:17 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux
```

Kernel option for SR-IOV (intel_iommu is required. Hugepages aren't).

```
$ cat /etc/default/grub  |grep CMD
GRUB_CMDLINE_LINUX="rd.lvm.lv=fedora_fedora/root rhgb quiet intel_iommu=on iommu=pt hugepages=2048"
```

Mellanox MFT must be installed.

Create VFs with the provided script (will launch mst):

```
sudo ./enable_vf.sh
```

If this is the first time this is executed (check for VF count change from 0 to 4), then follow the advice and reboot the node.

Check lspci for the Virtual Functions:

```
$ sudo lspci|grep Mell
01:00.0 Ethernet controller: Mellanox Technologies MT28800 Family [ConnectX-5 Ex]
01:00.1 Ethernet controller: Mellanox Technologies MT28800 Family [ConnectX-5 Ex]
01:00.2 Ethernet controller: Mellanox Technologies MT28800 Family [ConnectX-5 Ex Virtual Function]
01:00.3 Ethernet controller: Mellanox Technologies MT28800 Family [ConnectX-5 Ex Virtual Function]
01:00.4 Ethernet controller: Mellanox Technologies MT28800 Family [ConnectX-5 Ex Virtual Function]
01:00.5 Ethernet controller: Mellanox Technologies MT28800 Family [ConnectX-5 Ex Virtual Function]
```

```
$ ip link show dev enp1s0f0np0
4: enp1s0f0np0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc mq state UP mode DEFAULT group default qlen 1000
    link/ether 0c:42:a1:6a:8f:60 brd ff:ff:ff:ff:ff:ff
    vf 0     link/ether 00:00:00:00:00:00 brd ff:ff:ff:ff:ff:ff, spoof checking off, link-state auto, trust off, query_rss off
    vf 1     link/ether 02:11:11:11:11:11 brd ff:ff:ff:ff:ff:ff, spoof checking off, link-state auto, trust off, query_rss off
    vf 2     link/ether 02:22:22:22:22:22 brd ff:ff:ff:ff:ff:ff, spoof checking off, link-state auto, trust off, query_rss off
    vf 3     link/ether 00:00:00:00:00:00 brd ff:ff:ff:ff:ff:ff, spoof checking off, link-state auto, trust off, query_rss off
```

Create both alpine containers with the provided scripts. They will also configre VFs for each container and assign IP:

```
sudo ./create_alpine1.sh
sudo ./create_alpine2.sh
```

Check containers

```
$ docker ps
CONTAINER ID   IMAGE     COMMAND     CREATED          STATUS          PORTS     NAMES
0fb50f30555c   alpine    "/bin/sh"   17 minutes ago   Up 17 minutes             alpine2
7ad46e935d74   alpine    "/bin/sh"   22 minutes ago   Up 22 minutes             alpine1
```

```
$ docker exec alpine1 ifconfig enp1s0f0v1
enp1s0f0v1 Link encap:Ethernet  HWaddr 02:11:11:11:11:11  
          inet addr:172.22.0.1  Bcast:0.0.0.0  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:315 errors:0 dropped:0 overruns:0 frame:0
          TX packets:179 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:91114 (88.9 KiB)  TX bytes:28434 (27.7 KiB)

$ docker exec alpine2 ifconfig enp1s0f0v2
enp1s0f0v2 Link encap:Ethernet  HWaddr 5E:C9:1D:92:EC:2B  
          inet addr:172.22.0.2  Bcast:0.0.0.0  Mask:255.255.255.0
          UP BROADCAST RUNNING MULTICAST  MTU:1500  Metric:1
          RX packets:314 errors:0 dropped:0 overruns:0 frame:0
          TX packets:214 errors:0 dropped:0 overruns:0 carrier:0
          collisions:0 txqueuelen:1000 
          RX bytes:89656 (87.5 KiB)  TX bytes:34610 (33.7 KiB)
```

Install tcpdump in the containers with 'apk add tcpdump'.

Run tcpdump in alpine2 and ping from alpine1 to alpine2:

```
$ docker exec -ti alpine1 ping -c3 172.22.0.2
PING 172.22.0.2 (172.22.0.2): 56 data bytes
64 bytes from 172.22.0.2: seq=0 ttl=64 time=0.120 ms
64 bytes from 172.22.0.2: seq=1 ttl=64 time=0.272 ms
64 bytes from 172.22.0.2: seq=2 ttl=64 time=0.275 ms

--- 172.22.0.2 ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 0.120/0.222/0.275 ms
```

```
$ docker exec -ti alpine2 tcpdump -n -i enp1s0f0v2 -e
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp1s0f0v2, link-type EN10MB (Ethernet), snapshot length 262144 bytes
15:28:11.758292 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype IPv4 (0x0800), length 98: 172.22.0.1 > 172.22.0.2: ICMP echo request, id 59, seq 0, length 64
15:28:11.758302 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype IPv4 (0x0800), length 98: 172.22.0.2 > 172.22.0.1: ICMP echo reply, id 59, seq 0, length 64
15:28:12.758424 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype IPv4 (0x0800), length 98: 172.22.0.1 > 172.22.0.2: ICMP echo request, id 59, seq 1, length 64
15:28:12.758452 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype IPv4 (0x0800), length 98: 172.22.0.2 > 172.22.0.1: ICMP echo reply, id 59, seq 1, length 64
15:28:13.758554 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype IPv4 (0x0800), length 98: 172.22.0.1 > 172.22.0.2: ICMP echo request, id 59, seq 2, length 64
15:28:13.758580 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype IPv4 (0x0800), length 98: 172.22.0.2 > 172.22.0.1: ICMP echo reply, id 59, seq 2, length 64
^C
6 packets captured
6 packets received by filter
0 packets dropped by kernel
```

To validate no traffic is reaching the 2nd port via loopback cable, run tcpdump on the second port. As expected. No traffic arrives.

Whats not clear to me though, is where the multicast MAC address 5e:c9:1d:92:ec:2b comes from.... pinging in the other direction we see the same mac
address being used for both VFs as destination. 

```
$ docker exec -ti alpine2 ping -c3 172.22.0.1
PING 172.22.0.1 (172.22.0.1): 56 data bytes
64 bytes from 172.22.0.1: seq=0 ttl=64 time=0.109 ms
64 bytes from 172.22.0.1: seq=1 ttl=64 time=0.219 ms
64 bytes from 172.22.0.1: seq=2 ttl=64 time=0.204 ms

--- 172.22.0.1 ping statistics ---
3 packets transmitted, 3 packets received, 0% packet loss
round-trip min/avg/max = 0.109/0.177/0.219 ms
```

```
$ docker exec -ti alpine1 tcpdump -n -i enp1s0f0v1 -e
tcpdump: verbose output suppressed, use -v[v]... for full protocol decode
listening on enp1s0f0v1, link-type EN10MB (Ethernet), snapshot length 262144 bytes
15:30:06.571232 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype IPv4 (0x0800), length 98: 172.22.0.2 > 172.22.0.1: ICMP echo request, id 41, seq 0, length 64
15:30:06.571240 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype IPv4 (0x0800), length 98: 172.22.0.1 > 172.22.0.2: ICMP echo reply, id 41, seq 0, length 64
15:30:07.571326 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype IPv4 (0x0800), length 98: 172.22.0.2 > 172.22.0.1: ICMP echo request, id 41, seq 1, length 64
15:30:07.571345 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype IPv4 (0x0800), length 98: 172.22.0.1 > 172.22.0.2: ICMP echo reply, id 41, seq 1, length 64
15:30:08.571491 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype IPv4 (0x0800), length 98: 172.22.0.2 > 172.22.0.1: ICMP echo request, id 41, seq 2, length 64
15:30:08.571506 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype IPv4 (0x0800), length 98: 172.22.0.1 > 172.22.0.2: ICMP echo reply, id 41, seq 2, length 64
15:30:11.980288 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype ARP (0x0806), length 42: Request who-has 172.22.0.2 tell 172.22.0.1, length 28
15:30:11.980329 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype ARP (0x0806), length 60: Request who-has 172.22.0.1 tell 172.22.0.2, length 46
15:30:11.980341 02:11:11:11:11:11 > 5e:c9:1d:92:ec:2b, ethertype ARP (0x0806), length 42: Reply 172.22.0.1 is-at 02:11:11:11:11:11, length 28
15:30:11.980466 5e:c9:1d:92:ec:2b > 02:11:11:11:11:11, ethertype ARP (0x0806), length 60: Reply 172.22.0.2 is-at 5e:c9:1d:92:ec:2b, length 46
^C
10 packets captured
10 packets received by filter
0 packets dropped by kernel
```

## Conclusion

At least for Mellanox ConnectX-5, it is possible to communicate via NIC between VFs.

## Resources

- https://github.com/Mellanox/docker-sriov-plugin (not used here, but should provide similar results)
