#!/bin/bash

int=$1
container=$2
ipv4=$3
ipv6=$4

if [ -z "$ipv4" ]; then
  echo "sudo $0 <interface> <container> <ipv4/mask> [<ipv6/mask>]"
  exit 1
fi

if [ $(id -u) -ne 0 ]; then 
  echo "please run as root" 
  exit 1
fi

mkdir -p /var/run/netns
fc1=$(docker ps -q -f name=$container)
echo "$container $fc1"
pid1=$(docker inspect -f "{{.State.Pid}}" $fc1)
if [ -z "$pid1" ]; then
  echo "Can't find pid for container $container"
  exit 1
fi

echo "$container has pid $pid1"

ln -sf /proc/$pid1/ns/net /var/run/netns/$container

ip link set $int name $int netns $container

echo "setting ip addresses $ipv4 and $ipv6 on $int ..."
ip netns exec $container ip addr add $ipv4 dev $int
if [ ! -z "$ipv6" ]; then
  ip netns exec $container ip -6 addr add $ipv6 dev $int
fi

echo "enable interface $int"
ip netns exec $container ip link set up $int
