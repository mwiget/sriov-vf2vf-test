#!/bin/bash
PF=enp1s0f0np0
VF=enp1s0f0v2

if [ $(id -u) -ne 0 ]; then 
  echo "please run as root" 
  exit 1
fi

echo 4 > /sys/class/net/$PF/device/sriov_numvfs
echo -n $PF sriov_numvfs=
cat /sys/class/net/$PF/device/sriov_numvfs

ip link set $PF down
ip link set $PF vf 2 mac 02:22:22:22:22:22
ip link set $PF up
sleep 1
ip link show dev $PF

docker run -ti --rm -d --privileged --name alpine2 alpine
./move-link-netns.sh enp1s0f0v2 alpine2 172.22.0.2/24
